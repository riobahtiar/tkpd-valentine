(function($) {
  /* share button handler */
  $('.btn-share').click(function() {
      var image = $('.slick-active').children('img').attr('src');
      postToFeed(image);
  });
  /* save button handler */
  $('.btn-save').click(function() {
      var image = $('.slick-active').children('img').attr('src');
      $(this).attr('href', image);
  });
	// $ Works! You can test it with next line if you like
	// console.log($);
 $('.top-navigation-bar').click(function(){
   $('.overlay-on').fadeIn();
   $('.top-navigation-menu').fadeIn();
   $('body').addClass('body-fix');
 });
 // $('.overlay-on').click(function(){
 //   $('.top-navigation-menu').fadeOut();
 //   $('.overlay-on').fadeOut();
 //   $('body').removeClass('body-fix');
 //
 // });
 $('.top-navigation-menu a').click(function(){
    $('body').removeClass('body-fix');
 });
 $('.menu-close').click(function(){
   $('body').removeClass('body-fix');
   $('.top-navigation-menu').fadeOut();
   $('.overlay-on').fadeOut();

 });
 $(window).resize(function(){
  if($(this).innerWidth()>1000){
      $('.top-navigation-menu').fadeIn();
  }else{
     $('.top-navigation-menu').hide();
  }
 });

 /*=====================================
 =     Header script (Bridge Widget)  =
 =====================================*/
$(document).ready(function(e){
$("#more").click(function(){
  $('.more').slideToggle();
});


 $('html').on('click', function(e) {
     if ($('.bridge-widget').hasClass('active')) {
         $('.bridge-widget').removeClass('active');
     }
     if ($('.user-menu').hasClass('active')) {
         $('.user-menu').removeClass('active');
     }
 });

 /* toggle dropdown on button click */
 $('.bw-button').on('click', function(e) {
     if ($('.user-menu').hasClass('active')) {
         $('.user-menu').removeClass('active');
     }
     $(this).parent().toggleClass('active');

     /* hack for preventing html fire close event */
     e.stopPropagation();
 });

 /* hack for preventing html fire close event */
 $('.bw-container').on('click', function(e) {
     e.stopPropagation();
 });
});


})( jQuery );
