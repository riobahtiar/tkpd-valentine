<!-- Footer -->
<div class="footer">
  <div class="container">
    <div class="pull-left footer-copyright">
      &copy; 2009-2017, PT Tokopedia
    </div>                <div class="pull-right footer-connect">
      <ul>
        <li><a href="https://itunes.apple.com/us/app/tokopedia/id1001394201?ls=1&mt=8" target="_blank"><img class="footer-connect__apps" src="https://ecs7.tokopedia.net/assets/images/store/appstore.png" alt="appstore"></a></li>
        <li><a href="https://play.google.com/store/apps/details?id=com.tokopedia.tkpd" target="_blank"><img class="footer-connect__apps" src="https://ecs7.tokopedia.net/assets/images/store/googleplay.png" alt="playstore"></a></li>
      </ul>
      <ul>
        <li><a href="https://www.facebook.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/assets/images/social/facebook.png" alt="facebook"></a></li>
        <li><a href="http://instagram.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/assets/images/social/instagram.png" alt="instagram"></a></li>
        <li><a href="https://twitter.com/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/assets/images/social/twitter.png" alt="twitter"></a></li>
        <li><a href="https://www.youtube.com/user/tokopedia" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/assets/images/social/youtube.png" alt="youtube"></a></li>
        <li><a href="https://plus.google.com/+tokopedia/posts" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/assets/images/social/googleplus.png" alt="google_plus"></a></li>
        <li><a href="https://blog.tokopedia.com/" target="_blank"><img class="footer-connect__socmed" src="https://ecs7.tokopedia.net/assets/images/social/blog.png" alt="blog_tokopedia"></a></li>
      </ul>
    </div>            </div>
  </div>
  <!-- End Footer -->

  <?php wp_footer();?>
</body>
</html>
