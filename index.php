<?php get_header();?>
<div class="overlay_od_video" >
</div>

<!-- Banner -->
<div class="banner">

<div class="banner-overlay"></div>
  <!-- Top Navigation -->
  <div class="top-navigation">
    <div class="container">
      <div class="logo">
        <div class="top-navigation-bar"></div>

        <a href="<?php echo get_site_url(); ?>"><img src="<?php echo get_theme_mod('logo');?>" height="35"></img></a>

      </div>
      <div class="wrap_bridge-menu pull-right">

        <div class="bridge-widget inline-block">
          <button class="bw-button">Expander</button>
          <div class="bw-container">
            <div class="bw-item">
              <a href="https://www.tokopedia.com/">
                <div class="bw-icon__wrapper">
                  <div class="bw-icon bw-icon-toped">
                  </div>
                </div>
                <p class="bw-icon__text">
                  Jual Beli Online
                </p>
                <span class="clear-b"></span></a>
              </div>
              <div class="bw-item">
                <a href="https://tiket.tokopedia.com/kereta-api">
                  <div class="bw-icon__wrapper">
                    <div class="bw-icon bw-icon-tiket">
                    </div>
                  </div>
                  <p class="bw-icon__text">
                    Tiket
                  </p>
                  <span class="clear-b"></span></a>
                </div>
                <div class="bw-item">
                  <a href="https://pulsa.tokopedia.com/token-listrik/">
                    <div class="bw-icon__wrapper">
                      <div class="bw-icon bw-icon-listrik">
                      </div>
                    </div>
                    <p class="bw-icon__text">
                      Token Listrik
                    </p>
                    <span class="clear-b"></span></a>
                  </div>
                  <div class="bw-item">
                    <a href="https://pulsa.tokopedia.com/">
                      <div class="bw-icon__wrapper">
                        <div class="bw-icon bw-icon-pulsa">
                        </div>
                      </div>
                      <p class="bw-icon__text">
                        Pulsa
                      </p>
                      <span class="clear-b"></span></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="top-navigation-menu">
                <div class="menu-header">
                  MENU
                  <div class="pull-right">
                    <span class="menu-close">X</span>
                  </div>
                </div>
                <div class="menu-top-menu-container">
                  <ul class="menu-top-menu">
                    <li><a href="#" data-attr-scroll="section1" class="scrollto">ABOUT</a></li>
                    <li><a href="#" data-attr-scroll="section2" class="scrollto">KATAKAN CINTA</a></li>
                    <li><a href="#" data-attr-scroll="section3" class="scrollto">#KADOUNTUKDIA</a></li>
                    <li><a href="#" data-attr-scroll="section4" class="scrollto">CARA IKUTAN</a></li>
                    <li><a href="#" data-attr-scroll="section5" class="scrollto">HADIAH</a></li>
                    <li><a href="#" data-attr-scroll="section6" class="scrollto">SYARAT & KETENTUAN</a></li>
                  </ul>
                </div>
                <div class="clear"></div>
              </div>
            </div>

          </div>
          <!-- End Top Navigation -->
          <div class="banner_logo">

            <div class="container">
              <div class="text-center">
                <p>
                  <img src="<?php echo get_theme_mod('bannerlogo');?>" alt="" class="">
                </p>
                <p>
                  <img src="<?php echo get_theme_mod('playvideo');?>" alt="" class="playvideo">
                </p>
              </div>
            </div>
          </div>
</div>
<!-- Banner -->
<!-- Section One -->
<div class="section_one" id="section1">


    <div class="bg_section_one">
      <div class="container">
      <div class="section_content">
        <div class="row">
          <div class="col-md-5">
            <img src="<?php images('topedbook.png');?>" alt="" class="img-responsive">
          </div>
          <div class="col-md-7">
            <h2>Cinta Bisa Datang Kapan Saja, Dimana Saja.</h2>
            <p>Cinta bisa datang kapan saja, bahkan di saat yang tak terduga. Saat bertemu di jalan, bersapa di media sosial, termasuk saat kamu belanja di Tokopedia. Bisa jadi, di situlah takdir barumu dimulai. Karena cinta hanya menunggu waktu yang tepat untuk dipertemukan.</p>
          </div>
        </div>
      </div>
      </div>
  </div>
</div>
<!-- Section One -->
<!-- Section Two -->
<div class="section_two" id="section2">
  <div class="bg-section-two">
    <div class="container">
      <div class="booth_photo">

      <div class="text-center">
          <h2>Katakan Cintamu</h2>
      </div>
      <div class="slider-navigation">
        <div class="slider-left">
          <img src="<?php images('left.png');?>" alt="">
        </div>
        <div class="slider-right">
          <img src="<?php images('right.png');?>" alt="">
        </div>
      </div>
      <div class="slider">

      <div><img src="<?php images('quotes-02.jpg');?>" alt="" class="img-responsive"></div>
      <div><img src="<?php images('quotes-03.jpg');?>" alt="" class="img-responsive"></div>
      <div><img src="<?php images('quotes-04.jpg');?>" alt="" class="img-responsive"></div>
      <div><img src="<?php images('quotes-05.jpg');?>" alt="" class="img-responsive"></div>
      <div><img src="<?php images('quotes-06.jpg');?>" alt="" class="img-responsive"></div>
      <div><img src="<?php images('quotes-07.jpg');?>" alt="" class="img-responsive"></div>
      <div><img src="<?php images('quotes-08.jpg');?>" alt="" class="img-responsive"></div>
      </div>
      <div class="booth_btn">
        <div class="text-center">
  <a class="btn btn-primary btn-share" style="background:#f16980;border-color:#f16980;"><img src="<?php images('ic_facebook.png');?>" alt=""> Share</a>
  <a href="" download="TokopediaValentineQuotes.jpg" class="btn btn-primary btn-save" style="background:#f16980;border-color:#f16980;"><img src="<?php images('ic_save.png');?>" alt=""> Save</a>
</div>
      </div>
      <div class="baloon">
        <img src="<?php images('balon.png');?>" alt="">
      </div>
      </div>
      <div class="row" id="section3">
        <div class="col-md-7">
          <img src="<?php images('ic_akubeliin.png');?>" alt="" class="akubeliin">
          <h2>Bahagiakan Dia,
  Menangkan Hadiahnya</h2>
  <p>Ada banyak cara untuk mengungkapkan cinta kepada orang tersayang, salah satunya dengan memberikannya hadiah. Yuk, belikan kado impian untuk orang yang kamu sayang dan ikutan kompetisi #KadoUntukDia. Kamu bisa bikin dia bahagia sekaligus memenangkan berbagai hadiah menarik, lho.</p>
        </div>
        <div class="col-md-5">
            <div class="cloud-section">
              <img src="<?php images('toped3.png');?>" alt="">
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Section Two -->
<!-- Section Three -->
<div class="section_three" id="section4">
  <div class="container">
    <div class="text-center">
      <h2>Cara Ikutan:</h2>
    </div>
    <div class="row">
      <div class="col-md-4">
        <img src="<?php images('step1.png');?>" alt="">
        <p>Kamu bebas pilih produk apa aja di www.tokopedia.com (bukan pulsa atau tiket). Semua produk yang si dia mau, ada di Tokopedia!

</p>
      </div>
      <div class="col-md-4">
        <div class="add-margin"></div>
        <img src="<?php images('step2.png');?>" alt="">
        <p>Foto orang yang kamu sayang bersama kado pilihanmu. Makin menarik fotonya, makin besar pula kesempatanmu untuk menang.</p>
      </div>
      <div class="col-md-4">
        <img src="<?php images('step3.png');?>" alt="">
        <p>Upload ke akun Instagram kamu atau di Facebook Fanpage Tokopedia. Jangan lupa tuliskan nama toko, mention @tokopedia, dan pakai hashtag #KadoUntukDia #DimulaiDariTokopedia ya!</p>
      </div>
    </div>
  </div>
</div>
<!-- Section Three -->
<!-- Section Four -->
<div class="section_four" id="section5">
  <div class="container">
    <div class="text-center">
      <h2>Hadiah</h2>
    </div>
    <div class="row">
      <div class="col-md-4">
        <img src="<?php images('voucherbaru.png');?>" alt="" class="img-full">
        <h4>Pemenang Mingguan</h4>
        <p>Voucher Tokopedia Rp 250.000 untuk masing-masing pemenang</p>
        <p></p>
      </div>
      <div class="col-md-4">
        <div class="add-margin-top"></div>
        <img src="<?php images('cam.png');?>" alt="">
        <h4>Pemenang Utama (2 Orang Pemenang)</h4>
        <p>1. Fujifilm XA-2</p>
<p>2. Asus Zenfone 3</p>

      </div>
      <div class="col-md-4">
        <img src="<?php images('instax.png');?>" alt="" class="pentax">
        <h4>Pemenang Favorit
(3 Orang Pemenang)</h4>
        <p>Fujifilm Instax Mini 8</p>


      </div>
    </div>
    <?php $instagram=new WP_Query(array('post_type'=>'instagram','posts_per_page'=>'6'));?>
    <?php if($instagram->have_posts()):?>
    <div class="text-center">
      <h2 class="title-photo">Pemenang Mingguan</h2>
      <p>Ini dia beberapa foto yang sudah dikirim oleh para peserta. Ayo jangan mau kalah, segera upload foto si dia bersama kado pilihanmu!</p>
    </div>
  <?php endif;?>


  </div>
</div>
<!-- Section Four -->
<!-- Section Five -->
<?php if($instagram->have_posts()):?>
<div class="section_five">
  <div class="container">
    <div class="row">
      <?php while($instagram->have_posts()):$instagram->the_post();?>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="box-photo">

          <a href="<?php echo get_the_post_thumbnail_url();?>" class="fancybox">
<div class="box-photo-overlay">
  <div class="overlay-inner">
    <div class="pull-left">
      <h3><?php echo get_post_meta( get_the_ID(), 'nama', true ); ?></h3>
      <a href="<?php echo get_post_meta( get_the_ID(), 'linkpeserta', true ); ?>" class=""><?php echo get_post_meta( get_the_ID(), 'akunpeserta', true ); ?></a>
    </div>
    <div class="pull-right">
      <?php if(get_post_meta( get_the_ID(), 'linkpeserta', true )):?>
      <a href="<?php echo get_post_meta( get_the_ID(), 'linkpeserta', true ); ?>" class="view-post">Lihat Post</a>
    <?php endif;?>
    </div>
  </div>
</div>
            <?php the_post_thumbnail('full',array('class'=>'img-responsive'));?></a>


        </div>
      </div>
    <?php endwhile;?>




    </div>
  </div>
</div>
<?php endif;?>
<!-- Section Five -->
<!-- Section Six -->
<div class="section_six" id="section6">
    <div class="container">
      <div class="text-center">
        <h2>Syarat & Ketentuan</h2>
      </div>

      <div class="term-condition">
        <ol>

          <li>Kompetisi <strong>#KadoUntukDia</strong> berlangsung selama periode 9 - 26 Februari 2017 di Instagram @tokopedia dan Facebook Page Tokopedia.</li>
          <li>Peserta wajib like Facebook Page Tokopedia, follow Twitter @tokopedia dan Instagram @tokopedia.</li>

          <span class="more">
          <li>
            Tokopedia hanya akan menilai foto yang memenuhi kriteria berikut:
            <ul>
              <li> Diunggah ke akun Instagram peserta atau submit melalui Facebook di tkp.me/kadountukdia.</li>
              <li>Mention serta tag @tokopedia dan menggunakan hashtag <strong>#KadoUntukDia</strong> <strong>#DimulaiDariTokopedia</strong>.</li>
              <li>Mencantumkan nama dan/atau link toko yang menjual produk tersebut.</li>
            </ul>

          </li>
          <li>Peserta dapat mengunggah lebih dari 1 (satu) foto, namun wajib menggunakan foto dan caption yang berbeda-beda.</li>
          <li>Dengan mengikuti kompetisi ini, peserta memberikan lisensi tak terbatas kepada Tokopedia terkait segala hak cipta atas konten yang diunggah oleh peserta, termasuk namun tidak terbatas pada gambar, foto, desain grafis, maupun caption/deskripsi.</li>
          <li>Pemenang akan dibagi menjadi 3 (dua) kategori, yaitu:

<p>a. Pemenang Mingguan, yang berhak mendapatkan voucher Tokopedia masing-masing Rp 250.000 (10 orang/minggu) </p>
<p>b. Pemenang Utama (2 orang), yang berhak mendapatkan:</p>
  <ul>
    <li>Pemenang 1 (1 orang): Fujifilm XA-2;</li>
    <li>Pemenang 2 (1 orang): Asus zenfone 3 ze552kl ram 4gb - 64gb; atau</li>
  </ul>

<p>c. Pemenang Favorit (3 orang), yang berhak mendapatkan Instax Mini 8</p></li>
<li> Di akhir periode kompetisi, pihak Tokopedia akan memilih 5 orang dari seluruh peserta untuk menjadi pemenang utama.</li>
<li>Pemenang utama akan diumumkan pada 1 Maret 2017.</li>
<li>Para pemenang wajib melakukan konfirmasi data diri ke email quiz@tokopedia.com dengan mencantumkan data sebagai berikut:
Nama Akun Social Media
Nama Lengkap
Alamat Email Akun Tokopedia
Alamat Rumah
Nomor Telepon yang bisa dihubungi
Serta melampirkan KTP atau identitas diri</li>
<li>Pemenang diberi waktu maksimal 3x24 jam untuk melakukan konfirmasi data diri, terhitung sejak tanggal pengumuman pemenang. Jika tidak melakukan konfirmasi dalam kurun waktu tersebut, pihak Tokopedia berhak mendiskualifikasi pemenang.</li>
<li>Hadiah mingguan akan dikirim ke email masing-masing pemenang. Sedangkan hadiah utama dan hiburan akan dikirim ke alamat yang sesuai dengan data pemenang. </li>
<li>Hadiah akan dikirimkan setelah kelengkapan data pemenang diterima oleh Tokopedia dan dinyatakan valid.</li>
<li>Pemenang yang sudah dipilih oleh pihak Tokopedia bersifat mutlak serta tidak dapat diganggu gugat. Dan pihak Tokopedia berhak mendiskualifikasi jika ditemukan kecurangan yang dilakukan oleh pemenang.</li>
<li>Peserta tidak dipungut biaya apapun. Harap berhati-hati terhadap penipuan yang mengatasnamakan Tokopedia.</li>
<li>Peserta menyetujui bahwa segala sesuatu yang terkait dengan konten yang diunggah oleh peserta dalam kompetisi ini merupakan tanggung jawab peserta sepenuhnya.</li>
<li>Tokopedia berhak mempublikasikan nama, nama depan, alamat, foto atau testimonial pemenang untuk keperluan komunikasi atau iklan berkenaan dengan kompetisi ini di berbagai media penerbitan, tanpa memberi kontribusi apapun pada pemenang.</li>
</span>
        </ol>
        <div class="text-center">
          <a href="#section6" class="btn btn-primary btn-more btn-lg" id="more" style="background:#f16980;border-color:#f16980;">Tampilkan Semua</a>
        </div>
      </div>
    </div>
</div>
<!-- Section Six -->
<!-- Section Seven -->
<?php $toppicks=new WP_Query(array('post_type'=>'toppicks','posts_per_page'=>'4'));?>
<?php if($toppicks->have_posts()):?>
<div class="section_seven" id="section7">
  <div class="container">
    <div class="text-center">
      <h2>Top Picks</h2>
      <p>Masih bingung mau beli apa buat si doi? Intip rekomendasi produk-produk terbaru dari Tokopedia, yuk!</p>
    </div>
    <div class="row">
      <?php while($toppicks->have_posts()):$toppicks->the_post();?>
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="box-toppicks">
          <a href="<?php echo get_post_meta( get_the_ID(), 'link', true ); ?>" target="_blank"><?php the_post_thumbnail('full',array('class'=>'img-responsive'));?></a>
          <div class="box-toppicks-desc">
            <small>Mulai Dari</small>
            <h3><?php echo get_post_meta( get_the_ID(), 'price', true ); ?></h3>
          </div>
        </div>
      </div>
    <?php endwhile;?>

    </div>
    <div class="text-center">
      <a href="https://www.tokopedia.com/toppicks/" class="btn btn-green" target="_blank">Lihat Semua Produk</a>
    </div>
  </div>
</div>
<?php endif;?>
<!-- Section Seven -->
<!-- Section Eight -->
<div class="section_eight" id="section8">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <img src="<?php images('toped-bunga.png');?>" alt="" class="img-responsive">
      </div>
      <div class="col-md-8">
        <h2>Bunga Dari Toped</h2>
        <p>Tanggal 14 Februari nanti, Toped juga akan bagi-bagi 1.000 bunga di CGV Blitz Teras Kota dan Bekasi Cyber Park, lho. Jangan lupa mampir di booth Toped ya, Toppers!</p>
      </div>
    </div>

  </div>
</div>
<!-- Section Eight -->
<div class="footer-desc">

</div>
<?php get_footer();?>
