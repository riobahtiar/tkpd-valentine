<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:image" content="https://ecs7.tokopedia.net/valentine/wp-content/uploads/2017/02/14112414/KadoUntukDia_FB-.jpg"/>
  <meta property="og:url" content="https://tokopedia.com/campaign/valentine" />
  <meta content="Cinta Dimulai dari Tokopedia" property="og:title" />
  <meta content="Bikin Valentine jadi makin seru dengan ikutan #KadoUntukDia yuk. Kamu bisa bikin dia happy sekaligus menangin Fujifilm XA-2, Asus Zenfone 3, dan voucher belanja." property="og:description" />
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <?php wp_head();?>
  <style media="screen">
  .banner{
    background:url('<?php echo get_theme_mod('banner');?>')no-repeat;
    height:800px;
    background-attachment: fixed;
    background-size: 100%;
    position:relative;
  }
  </style>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script>
    window.fbAsyncInit = function(){
    FB.init({
        appId: '1637365853225887', status: true, cookie: true, xfbml: true });
    };
    (function(d, debug){var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if(d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id;
        js.async = true;js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
        ref.parentNode.insertBefore(js, ref);}(document, /*debug*/ false));

    function postToFeed(image){
        var obj = {
            method: 'feed',
            link: 'https://tokopedia.com/campaign/valentine',
            picture: image,
            name: 'Tokopedia Valentine',
            description: 'Belanja murah dengan promo valentine dari Tokopedia'
        };
        function callback(response){}
        FB.ui(obj, callback);
    }
  </script>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TZNNXTG');</script>
  <!-- End Google Tag Manager -->

</head>
<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZNNXTG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="overlay-on">

  </div>
  <!-- Navigation -->
